package com.hojoki.chomei;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BitbucketActivity {

	private String subject;
	private String verb;
	private String object;
	private String timestamp;
	
	public BitbucketActivity (String sub, String verb, String obj) {
		
		Date d = new Date();
		String ts = new SimpleDateFormat("yyyyy-mm-dd hh:mm:ss").format(d);
		setSubject(sub);
		setVerb(verb);
		setObject(obj);
		setTimestamp(ts);	
		
	}
	
	public BitbucketActivity (String sub, String verb, String obj, String ts) {
		
		setSubject(sub);
		setVerb(verb);
		setObject(obj);
		setTimestamp(ts);	
		
	}
	
	public String getSubject () {
		return subject;
	}
	
	public void setSubject (String sub) {
		subject = sub;
	}
	
	public String getVerb () {
		return verb;
	}
	
	public void setVerb (String v) {
		verb = v;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String obj) {
		object = obj;
	}
	
	public String getTimestamp () {
		return timestamp;
	}
	
	public void setTimestamp (String ts) {
		timestamp = ts;
	}	
	
}
