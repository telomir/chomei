package com.hojoki.chomei;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import com.ning.http.client.oauth.ConsumerKey;
import com.ning.http.client.oauth.OAuthSignatureCalculator;
import com.ning.http.client.oauth.RequestToken;

public class BitbucketDanceMachine {
	
	private static final Logger logger = LoggerFactory.getLogger(BitbucketDanceMachine.class);
	private static final Boolean debug = true;
	
	private static BitbucketDanceMachine Instance;
	private static HttpSession Session;

	private static final String CONSUMER_KEY = "ve82RZGsmcAgFax97W";
	private static final String CONSUMER_SECRET = "8kGjPWFfMXeFBuUb5fPZPLcLRVQL3tw6";
	private static final String API_BASE_URL = "https://bitbucket.org/!api/1.0/";
	
	private static String REQUEST_TOKEN = "";
	private static String REQUEST_TOKEN_SECRET = "";
	private static String ACCESS_TOKEN = "";
	private static String ACCESS_TOKEN_SECRET = "";
	private static String VERIFIER = "";
	
	private static String Actor = "";
	private static String ActorName = "";
	
	private static ConsumerKey ConsKey;
	private static RequestToken ReqToken;
	private static OAuthSignatureCalculator SigCalc;
	private static AsyncHttpClient AsyncClient;
	
	public static List<BitbucketActivity> activities = new ArrayList<BitbucketActivity>();
	public static List<BitbucketRepository> repositories = new ArrayList<BitbucketRepository>();
	
	private BitbucketDanceMachine () {

	}
	
	public static BitbucketDanceMachine getInstance(HttpSession s) {
		if(Instance == null)
			 Instance = new BitbucketDanceMachine();
		
		if(Session == null) {
			Session = s;			
			REQUEST_TOKEN = (String) ((s.getAttribute("REQUEST_TOKEN") != null) ? s.getAttribute("REQUEST_TOKEN") : "");
			REQUEST_TOKEN_SECRET = (String) ((s.getAttribute("REQUEST_TOKEN_SECRET") != null) ? s.getAttribute("REQUEST_TOKEN_SECRET") : "");
			ACCESS_TOKEN = (String) ((s.getAttribute("ACCESS_TOKEN") != null) ? s.getAttribute("ACCESS_TOKEN") : "");
			ACCESS_TOKEN_SECRET = (String) ((s.getAttribute("ACCESS_TOKEN_SECRET") != null) ? s.getAttribute("ACCESS_TOKEN_SECRET") : "");
			VERIFIER = (String) ((s.getAttribute("VERIFIER") != null) ? s.getAttribute("VERIFIER") : "");

			Actor = (String) ((s.getAttribute("Actor") != null) ? s.getAttribute("Actor") : "");
			ActorName = (String) ((s.getAttribute("ActorName") != null) ? s.getAttribute("ActorName") : "");
			repositories = (List<BitbucketRepository>) ((s.getAttribute("repositories") != null) ? s.getAttribute("repositories") : repositories);
			activities = (List<BitbucketActivity>) ((s.getAttribute("activities") != null) ? s.getAttribute("activities") : activities);
		}
		
		ConsKey = new ConsumerKey(CONSUMER_KEY, CONSUMER_SECRET);
		if(ACCESS_TOKEN_SECRET.isEmpty()) {
			ReqToken = new RequestToken(REQUEST_TOKEN, REQUEST_TOKEN_SECRET);
			logger.info("Using RequestToken");
		}
		else {
			ReqToken = new RequestToken(ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
			logger.info("Using AccessToken");
		}
		SigCalc = new OAuthSignatureCalculator(ConsKey, ReqToken);
		AsyncClient = new AsyncHttpClient();
		
		return Instance;
	}
	
	public BitbucketDanceMachine fetchToken () {
		
		// Falls neuer Tokenfetch-Versuch gestartet wird, obwohl Acces-Token-Secret
		// schon da ist, dann verwerfe alle Tokens und Secrets und fange neu an
		if(!ACCESS_TOKEN_SECRET.isEmpty()) {
			setRequestToken("");
			setRequestTokenSecret("");
			setAccessToken("");
			setAccessTokenSecret("");
			setVerifier("");
			ReqToken = new RequestToken(REQUEST_TOKEN, REQUEST_TOKEN_SECRET);
			SigCalc = new OAuthSignatureCalculator(ConsKey, ReqToken);
		}
		
		try {
			// Entweder haben wir schon einen Request-Token, dann wollen wir den Access-Token,
			// ansonsten benoetigen wir zun�chst den Request-Token
			String phase = (REQUEST_TOKEN.isEmpty()) ? "oauth/request_token" : "oauth/access_token";
			
			// Zur Vereinfachung werden einfach bei beiden Token-Requests Callback-URI und Verifier
			// mitgesendet.
			Response response = AsyncClient
									.preparePost(API_BASE_URL + phase)
									.addParameter("oauth_callback", "http://localhost/Chomei/Access")
									.addParameter("oauth_verifier", VERIFIER)
									.setSignatureCalculator(SigCalc)
									.execute()
									.get();
	
			try {
				// Daten der Antwort verarbeiten
				Map<String, String> parameters = new HashMap<String, String>();
				String[] r = response.getResponseBody().split("&");
				for(int i = 0; i < r.length; i++) {
					String[] p = r[i].split("=");
					parameters.put(p[0], p[1]);
				}

				if(REQUEST_TOKEN.isEmpty()) {
					// Request-Token erhalten und ablegen
					setRequestToken(parameters.get("oauth_token"));
					setRequestTokenSecret(parameters.get("oauth_token_secret"));	
				}
				else {
					// Access-Token erhalten und daher Request-Token verwerfen
					setRequestToken("");
					setRequestTokenSecret("");	
					setAccessToken(parameters.get("oauth_token"));
					setAccessTokenSecret(parameters.get("oauth_token_secret"));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return this;		
	}
	
	public void fetchUserData ()  {

		try {
			Response response = fetchData("user");
			
			if(response != null) {				
				JSONObject obj = (JSONObject) JSONValue.parse(response.getResponseBody());
				JSONObject usr = (JSONObject) obj.get("user");
				
				setActor((String) usr.get("username"));
				setActorName(usr.get("first_name") + " " + usr.get("last_name"));
			}	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void importActivitiyHistory() {

		logger.info("Importing activity history ...");
		
		try {
			Response userData = fetchData("user");
		
			if(userData != null) {
				JSONObject obj = (JSONObject) JSONValue.parse(userData.getResponseBody());
				JSONObject usr = (JSONObject) obj.get("user");
				JSONArray repositories = (JSONArray) obj.get("repositories");
				
				Iterator<JSONObject> iterRepos = repositories.iterator();
				while(iterRepos.hasNext()) {
					JSONObject currentRepository = iterRepos.next();
					addActivity(new BitbucketActivity(	Actor,
															"createdRepos",
															(String) currentRepository.get("resource_uri"),
															(String) currentRepository.get("created_on")
														)
					);
					addRepository(new BitbucketRepository((String) currentRepository.get("slug")));
					logger.info("Repository '" + currentRepository.get("slug") + "' added to watchlist.");
				}
			}
			
			checkRepositoriesForUpdates();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void checkRepositoriesForUpdates () {
		
		try {
			Iterator<BitbucketRepository> iterRepos = repositories.iterator();
			while(iterRepos.hasNext()) {
				BitbucketRepository currentRepository = iterRepos.next();
				logger.info("Checking Repository '" + currentRepository.getSlug() + "' ...");
								
				Response repoEvents = fetchData("repositories/" + Actor + "/" + currentRepository.getSlug() + "/events?limit=50");
				
				if(!repoEvents.hasResponseBody()) {
					logger.info("Found no new events in '" + currentRepository.getSlug() + "'");
				}
				else {
					JSONObject eventWrapper = (JSONObject) JSONValue.parse(repoEvents.getResponseBody());
					JSONArray events = (JSONArray) eventWrapper.get("events");
					Iterator<JSONObject> eventsIterator = events.iterator();
					String lastTS = "";
					while(eventsIterator.hasNext()) {
						JSONObject event = eventsIterator.next();
						
						// Falls die letzte bekannte Node gefunden wurde, brich bearbeitung ab
						if(event.get("created_on").equals(currentRepository.getLastTimestamp())) {
							logger.info("Found old activity. Aborting.");
							break;
						}						
						addActivity(
								new BitbucketActivity(	(String) ((Map) event.get("user")).get("username"),
														(String) event.get("event") + " (" + (String) event.get("description") + ")",
														currentRepository.getSlug() + "#" + event.get("node"),
														(String) event.get("created_on")
													)
						);
						lastTS = (lastTS == "") ? (String) event.get("created_on") : lastTS;
						logger.info("Found new Activity in '" + currentRepository.getSlug() + "': " + event.get("event") + " (" + event.get("description") + ")");						
					}
					
					if(!lastTS.isEmpty()) {
						currentRepository.setLastTimestamp(lastTS);
						logger.info("Last timestamp for '" + currentRepository.getSlug() + "' set to '" + lastTS + "'");
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Response fetchData (String endpoint) {
		
		try {
			BitbucketRequestHandler<Response> bbrh = new BitbucketRequestHandler<Response>();
			Response response = AsyncClient
									.prepareGet(API_BASE_URL + endpoint)
									.setSignatureCalculator(SigCalc)
									.execute(bbrh)
									.get();
			
			return response;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	private void setRequestToken (String key) {
		REQUEST_TOKEN = key;
		Session.setAttribute("REQUEST_TOKEN", REQUEST_TOKEN);	
		
		if(debug)
			logger.info("RequestToken set to '" + key + "'");	
	}

	public String getRequestToken () {			
		return REQUEST_TOKEN;
	}
	
	private void setRequestTokenSecret (String sec) {
		REQUEST_TOKEN_SECRET = sec;
		Session.setAttribute("REQUEST_TOKEN_SECRET", REQUEST_TOKEN_SECRET);
		
		if(debug)
			logger.info("RequestTokenSecret set to '" + sec + "'");
	}
	
	public void setAccessToken (String at) {
		ACCESS_TOKEN = at;
		Session.setAttribute("ACCESS_TOKEN", ACCESS_TOKEN);
		
		if(debug)
			logger.info("AccesToken set to '" + at + "'");
	}

	public String getAccessToken () {			
		return ACCESS_TOKEN;
	}
	
	public void setAccessTokenSecret (String ats) {
		ACCESS_TOKEN_SECRET = ats;
		Session.setAttribute("ACCESS_TOKEN_SECRET", ACCESS_TOKEN_SECRET);
		
		if(debug)
			logger.info("AccesTokenSecret set to '" + ats + "'");
	}
	
	public void setVerifier (String v) {
		VERIFIER = v;
		Session.setAttribute("VERIFIER", VERIFIER);	
		
		if(debug)
			logger.info("Verifier set to '" + v + "'");
	}

	public String getVerifier () {			
		return VERIFIER;
	}
	
	public void setActor (String a) {
		Actor = a;
		Session.setAttribute("Actor", Actor);	
		
		if(debug)
			logger.info("Actor set to '" + a + "'");
	}

	public String getActor () {			
		return Actor;
	}
	
	public void setActorName (String an) {
		ActorName = an;
		Session.setAttribute("ActorName", ActorName);	
		
		if(debug)
			logger.info("ActorName set to '" + an + "'");
	}

	public String getActorName () {			
		return ActorName;
	}

	public List<BitbucketActivity> getActivityList() {
		return activities;
	}

	public void checkForUpdates() {
		checkRepositoriesForUpdates ();		
	}
	
	private void addActivity (BitbucketActivity a) {
		activities.add(a);
		Session.setAttribute("activities", activities);		
	}
	
	private void addRepository (BitbucketRepository r) {
		repositories.add(r);
		Session.setAttribute("repositories", repositories);		
	}


}
