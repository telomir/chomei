package com.hojoki.chomei;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitbucketPollingDaemon implements Runnable {
	
	private static final Logger logger = LoggerFactory.getLogger(BitbucketPollingDaemon.class);
	private static final int INTERVAL = 60;
	private static BitbucketDanceMachine bbdm = null;
	
	public BitbucketPollingDaemon (BitbucketDanceMachine dm) {
		bbdm = dm;
	}	
	
	public void checkForUpdates () {
		logger.info("Checking for Updates ...");
		bbdm.checkForUpdates();
	}

	public void run() {		
		while(!Thread.currentThread().isInterrupted()) {			
			try {
				Thread.sleep(INTERVAL * 1000);
				checkForUpdates();
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}
	}
}
