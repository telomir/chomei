package com.hojoki.chomei;

public class BitbucketRepository {

	private String slug = "";
	private String lastTimestamp = "";
	
	public BitbucketRepository (String slug) {
		setSlug(slug);
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getLastTimestamp() {
		return lastTimestamp;
	}

	public void setLastTimestamp(String lastTimestamp) {
		this.lastTimestamp = lastTimestamp;
	}
	
}
