package com.hojoki.chomei;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ning.http.client.AsyncHandler;
import com.ning.http.client.HttpResponseBodyPart;
import com.ning.http.client.HttpResponseHeaders;
import com.ning.http.client.HttpResponseStatus;
import com.ning.http.client.Response;

public class BitbucketRequestHandler<T> implements AsyncHandler<T> {

	private static final Logger logger = LoggerFactory.getLogger(BitbucketRequestHandler.class);
	private static final Boolean debug = false;
	private final Response.ResponseBuilder builder = new Response.ResponseBuilder();
	
	/**
	 * Status-Code erhalten
	 * 
	 * Sofern dieser gr��er oder gleich 400 ist, breche Abfrage ab.
	 * 
	 * @param	HttpResponseStatus status
	 * @return	STATE
	 */	
	public STATE onStatusReceived(HttpResponseStatus status) throws Exception {
		int statusCode = status.getStatusCode();
		if(statusCode < 400) {
			builder.accumulate(status);
			logger.info("StatusCode '" + statusCode +"' received. Continuing.");
			return STATE.CONTINUE;
		}
		else {
			logger.info("StatusCode '" + statusCode +"' received. Aborting.");
			return STATE.ABORT;
		}
	}
	
	/**
	 * Header erhalten
	 * 
	 * Eigentlich w�re es an dieser Stelle sch�n, wenn man den Last-Modified-Header
	 * auswerten k�nnte, ob sich etwas an den Daten ge�ndert hat. Leider schickt Bitbucket
	 * dar�ber aber nur den aktuellen Timestamp
	 * TODO Last-Modified-Header
	 * 
	 * @param	HttpResponseHeader headers
	 * @return	STATE
	 */
	public STATE onHeadersReceived(HttpResponseHeaders headers) throws Exception {
		
		// Gib Headerdaten aus, falls Debug-Ausgaben erwuenscht sind
		if(debug) {
			logger.info("Headers of Response received.");
			Iterator<Entry<String, List<String>>> i = headers.getHeaders().iterator();
			while(i.hasNext()) {
				Entry<String, List<String>> e = i.next();
				String key = (String) e.getKey();
				List<String> values = e.getValue();
				Iterator<String> j = values.iterator();
				while(j.hasNext()) {
					String value = j.next();
					logger.info(key + ": " + value);
				}			
			}
		}
		
		builder.accumulate(headers);
		return STATE.CONTINUE;
	}
	
	/**
	 * Teil des Bodys erhalten
	 * 
	 * Daten einfach im Responsebuilder akkumulieren
	 * 
	 * @param	HttpResponseBodyPart bp
	 * @return	STATE
	 */
	public STATE onBodyPartReceived(HttpResponseBodyPart bp) throws Exception {
		// Gib Meldung aus, falls Debug-Ausgaben erwuenscht sind
		if(debug)
			logger.info("BodyPart received.");
		
		builder.accumulate(bp);
		return STATE.CONTINUE;
	}

	/**
	 * Request fertig
	 * 
	 * Response fertig basteln.
	 * 
	 * @return Response
	 */
	
	public T onCompleted() throws Exception {
		// Gib Meldung aus, falls Debug-Ausgaben erwuenscht sind
		if(debug)
			logger.info("Request completed.");
		
		return (T) builder.build();
	}

	public void onThrowable(Throwable throwable) {
		logger.info("Throwable: " + throwable.getMessage());		
	}

}
