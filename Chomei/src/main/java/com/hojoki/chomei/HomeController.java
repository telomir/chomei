package com.hojoki.chomei;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private static Thread Daemon = null;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
				
		logger.info("Hallo Hojoki-Team!");
		
		return "home";
	}
	
	@RequestMapping(value="Authenticate")
	public String authenticate (HttpSession sess) {
		BitbucketDanceMachine bbdm = BitbucketDanceMachine
										.getInstance(sess)
										.fetchToken();
		String RequestToken = bbdm.getRequestToken(); 
		
		logger.info("Redirecting to Bitbucket.");
		return "redirect:https://bitbucket.org/!api/1.0/oauth/authenticate?oauth_token=" + RequestToken; 
	}
	
	@RequestMapping(value="Access")
	public String access (HttpSession sess, HttpServletRequest request) {

		BitbucketDanceMachine bbdm = BitbucketDanceMachine.getInstance(sess);
		bbdm.setAccessToken(request.getParameter("oauth_token"));
		bbdm.setVerifier(request.getParameter("oauth_verifier"));
		bbdm.fetchToken();	
		
		return "redirect:ActivityStream"; 	
	}
	
	@RequestMapping(value="ActivityStream")
	public ModelAndView activityStream (HttpSession sess, Model model) {

		BitbucketDanceMachine bbdm = BitbucketDanceMachine.getInstance(sess);

		Daemon = (Thread) sess.getAttribute("POLLING_DAEMON");
		if(Daemon == null) {
			Daemon = new Thread(new BitbucketPollingDaemon(bbdm));
			Daemon.setDaemon(true);
			sess.setAttribute("POLLING_DAEMON", Daemon);
			Daemon.start();
		}
		
		List<BitbucketActivity> activityStream = bbdm.getActivityList();
		if(activityStream.size() == 0) {
			bbdm.fetchUserData();
			bbdm.importActivitiyHistory();
		} 
		else
			logger.info("Skipping import of activity history ...");
		
		model.addAttribute("ActorName", bbdm.getActorName());
		model.addAttribute("activityStream", activityStream);
		
		return new ModelAndView("activityStream");
	}
	
	@RequestMapping(value="Goodbye")
	public String goodbye (HttpSession sess) {
		Daemon = (Thread) sess.getAttribute("POLLING_DAEMON");
		Daemon.interrupt();
		logger.info("Time to say 'Goodbye!'");
		//sess.invalidate();
		
		return "home";
	}
	
}
