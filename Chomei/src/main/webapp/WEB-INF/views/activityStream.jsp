<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Chomei: ActivityStream (${ActorName})</title>
</head>
<body>
<h1>
	Hallo ${ActorName}!
</h1>
<c:forEach var="activity" items="${activityStream}">
<p>(${activity.timestamp}) ${activity.subject} - ${activity.verb} -  ${activity.object}.</p>
</c:forEach>

<input type="button" value="Goodbye" onclick="location.href = 'Goodbye';" />
</body>
</html>
